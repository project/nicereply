-- SUMMARY --

This module allows to rate node using Nicereply service:
https://www.nicereply.com
Nicereply is an easy to use tool that lets you collect and analyze email
feedback and improve your customer experience. It provides good API so
I have thought it may be useful for someone to use it for rating Drupal
nodes as well.

-- INSTALLATION --

As any other module - just copy it to the sites/all/modules directory
and enable either via user interface or via drush (drush en nicereply).

-- REQUIREMENTS --

None.

-- CONFIGURATION --

You have to get your API key on Nicereply.com service page. You have to
enter API credentials in this module configuration form on admin pages in
Configuration -> Web Services -> Nicereply (admin/config/services/nicereply).

-- THANKS --

The development of this module is sponsored by Websupport.sk company.
http://websupport.sk
