<?php

/**
 * @file
 * Default theme implementation to display an users statistics.
 *
 * Available variables:
 * - $stats: An array containing user's Nicereply statistics. Check
 * http://www.nicereply.com/API-example/doc/api-reference.html#getuserstats
 * for more information.
 */
?>
<h3>User Statistics</h3>
<div class="user-stats">
<?php
  $header = array(t('Parameter'), t('Value'));
  $rows = array(
    array(t('User Average'), $stats->userAverage),
    array(t('User Ratings Count'), $stats->userRatingsCount),
    array(t('User Average Last Month'), $stats->userAverageLastMonth),
    array(t('User Ratings Count Last Month'), $stats->userRatingsCountLastMonth),
    array(t('User Ratings Difference'), $stats->userRatingsDifference),
  );
  print theme('table', array(
    'header'  => $header,
    'rows'    => $rows,
  ));
?>
</div>
