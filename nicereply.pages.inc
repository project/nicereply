<?php

/**
 * @file
 * Pages include file for Nicereply module.
 */

/**
 * User settings form.
 */
function nicereply_user_settings_form($form, &$form_state) {
  $form = array();

  // For code reviewers: This API call gets the list of Nicereply users.
  // For others: no need to comment, we all understand.
  $result = nicereply_api_call('getUserList', array());

  if (isset($result->users)) {
    $users = array(0 => t('-- Not linked --'));
    foreach ($result->users as $u) {
      $users[$u->id] = $u->username . ' (' . $u->fullname . ')';
    }

    if ((arg(0) == 'user') && (is_numeric(arg(1)))) {
      $user = user_load(arg(1));
      if (nicereply_get_nicereply_user($user->uid)) {
        $stats = theme('nicereply_user_stats', array('stats' => nicereply_api_call('getUserStats', array('userid' => $user->uid))));
        $form['nicereply_stats'] = array(
          '#markup'     => $stats,
        );
      }

      if (nicereply_user_settings_access(arg(1))) {

        $form['nicereply_link'] = array(
          '#title'      => t('Link user to Nicereply account'),
          '#type'       => 'fieldset',
          '#collapsed'  => TRUE,
          '#collapsible' => TRUE,
        );

        $form['nicereply_link']['nicereply_user'] = array(
          '#type'       => 'select',
          '#title'      => t('Nicereply user'),
          '#description' => t('Select Nicereply user to which %user will be linked. Only users which have been rated at least once are listed.', array('%user' => $user->name)),
          '#options'    => $users,
          '#required'   => FALSE,
          '#default_value' => nicereply_get_nicereply_user(arg(1)),
        );

        $form['nicereply_link']['submit'] = array(
          '#type'   => 'submit',
          '#value'  => t('Link account'),
        );
      }
    }
  }
  else {
    $form['nicereply_error'] = array(
      '#markup'     => t('Unable to retrieve list of users. Please, contact system administrator.'),
    );
  }
  return $form;
}

/**
 * Submit handler for user settings form.
 */
function nicereply_user_settings_form_submit($form, &$form_state) {
  if ((arg(0) == 'user') && (is_numeric(arg(1)))) {
    $uid = arg(1);
    $result = db_select('nicereply_users', 'nu')
      ->fields('nu')
      ->condition('uid', $uid, '=')
      ->execute()
      ->fetchAssoc();
    if ($result) {
      $response = db_update('nicereply_users')
        ->fields(array(
          'nr_uid' => $form_state['values']['nicereply_user'],
          )
        )
        ->condition('uid', $uid, '=')
        ->execute();
    }
    else {
      $response = db_insert('nicereply_users')
        ->fields(
          array(
            'nr_uid'  => $form_state['values']['nicereply_user'],
            'uid'     => $uid,
          )
        )
        ->execute();
    }
    if ($response) {
      drupal_set_message(t('User has been assigned.'), 'status');
    }
  }
}
