<?php
/**
 * @file
 * When Nicereply and Drupal meet.
 */

/**
 * Admin settings for Nicereply module.
 */
function nicereply_admin_settings() {

  $form = array();

  $form['keys'] = array(
    '#type'       => 'fieldset',
    '#title'      => t('Nicereply keys'),
    '#collapsible' => FALSE,
    '#collpapsed' => FALSE,
  );

  $form['keys']['nicereply_public_key'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Nicereply Public Key'),
    '#default_value'  => variable_get('nicereply_public_key', NULL),
    '#description'    => t('Enter the Nicereply public key'),
    '#required'       => TRUE,
  );

  $form['keys']['nicereply_private_key'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Nicereply Private Key'),
    '#default_value'  => variable_get('nicereply_private_key', NULL),
    '#description'    => t('Enter the Nicereply private key'),
    '#required'       => TRUE,
  );

  $form['path'] = array(
    '#type'       => 'fieldset',
    '#title'      => t('Paths'),
    '#collapsible' => FALSE,
    '#collpapsed' => FALSE,
  );

  $form['path']['nicereply_api_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Nicereply API Path'),
    '#default_value'  => variable_get('nicereply_api_path', 'http://www.nicereply.com/api/'),
    '#description'    => t('Enter the Nicereply API path'),
    '#required'       => TRUE,
  );

  $form['node'] = array(
    '#type'       => 'fieldset',
    '#title'      => t('Node settings'),
    '#collapsible' => FALSE,
    '#collpapsed' => FALSE,
  );

  return system_settings_form($form);
}
